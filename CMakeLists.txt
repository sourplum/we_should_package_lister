cmake_minimum_required(VERSION 2.6)
project(we_should_package_lister)

add_executable(we_should_package_lister main.c)

install(TARGETS we_should_package_lister RUNTIME DESTINATION bin)
