#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define BUFFER_SIZE 256

enum distribution { ARCH, DEBIAN, RPM_BASED, GENTOO };

/**
 * @brief Hackish way to get the distribution's name
 * 
 * @return distribution the name of the current distro as an enum
 */
enum distribution get_current_distribution() {
    FILE* info_file;
    char distrib_id[8];
    enum distribution current_distribution;

    if (system("cat /etc/os-release | grep 'ID' | sed 's/.*ID=//' > /tmp/distrib.info") == -1) {
        perror("Error during system call\n");
        exit(EXIT_FAILURE);
    } else {
        info_file = fopen("/tmp/distrib.info", "r");
        fscanf(info_file, "%s", distrib_id);
        fclose(info_file);

        printf("Gnu/Linux distribution detected: %s\n", distrib_id);

        if(strcmp(distrib_id, "arch") == 0)
            current_distribution = ARCH;
        if(strcmp(distrib_id, "debian") == 0 || strcmp(distrib_id, "ubuntu") == 0)
            current_distribution = DEBIAN;
        if(strcmp(distrib_id, "fedora") == 0 || strcmp(distrib_id, "rhel") == 0 || strcmp(distrib_id, "opensuse") == 0)
            current_distribution = RPM_BASED;
        if(strcmp(distrib_id, "gentoo") == 0)
            current_distribution = GENTOO;
    }
    return current_distribution;
}

/**
 * @brief Use the right package manager for the job.
 * 
 * @param my_distrib p_my_distrib: current distribution used
 */
void package_manager(enum distribution my_distrib) {
    switch(my_distrib){
        case ARCH:
            /* pacman -Q */
            execlp("pacman", "pacman", "-Q", NULL);
            break;
        case RPM_BASED:
            /* rpm -qa */
            execlp("rpm", "rpm", "-qa", NULL);
            break;
        case DEBIAN:
            /* dpkg -l */
            execlp("dpkg", "dpkg", "-l", NULL);
            break;
        case GENTOO:
            /* emerge -e world */
            execlp("emerge", "emerge", "-e", "world", NULL);
            break;
    }
}

/**
 * @brief An effort by Cedric Bonhomme
 * 
 * @param argc p_argc:...
 * @param argv p_argv:...
 * @return int
 */
int main(int argc, char **argv) {
    pid_t child_pid;
    int nbytes;
    int output_file;
    int pipe_fd[2];
    char read_buffer[BUFFER_SIZE];
    enum distribution my_distrib;

    output_file = open("packages.list", O_CREAT | O_WRONLY, S_IRWXU);
    my_distrib = get_current_distribution();

    pipe(pipe_fd);

    if((child_pid = fork()) == -1) {
        perror("Error during fork\n");
        exit(EXIT_FAILURE);
    }
    if(child_pid == 0) {
        /* son launches the command and outputs to pipe */
        close(pipe_fd[0]);
        dup2(pipe_fd[1], STDOUT_FILENO);
        package_manager(my_distrib);
        close(pipe_fd[1]);
    } else {
        /* father reads from the pipe and writes to file */
        close(pipe_fd[1]);
        while((nbytes = read(pipe_fd[0], read_buffer, sizeof(read_buffer))) > 0) {
            write(output_file, read_buffer, nbytes);
        }
        printf("List of packages generated under packages.list\n");
        close(pipe_fd[0]);
        while(waitpid(-1, NULL, WNOHANG) != -1);
        close(output_file);
    }
    exit(EXIT_SUCCESS);
}
